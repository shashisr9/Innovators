﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace LinersChatAutomation.PageObjects
{
    public class clsLoginClass
    {
        IWebDriver _webDriver;

        public IWebElement txtUserName
        {
            get
            {
                return _webDriver.FindElement(By.Id("field-loginEmail"));
            }
        }

        public clsLoginClass(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public void LoginDetails()
        {
            //Enter user name in username text box
            txtUserName.SendKeys("asr.rajesh@gmail.com");
            Console.WriteLine("User name is entered in username text box");

            //Click on login button
            Console.WriteLine("Login button clicked");
        }
    }
}
