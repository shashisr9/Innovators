package com.innovators.LinusApp.Repository;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.innovators.LinusApp.Entities.Group;

@Repository
public interface LinusAppRepository extends JpaRepository<Group, Integer> {
	
	@Query(value="select * from group g where g.groupid = :groupid", nativeQuery = true)
	Stream<Group> findGroup(@Param("groupid") Integer groupid);
	
	@Query(value="select * from group g", nativeQuery = true)
	Stream<Group> findAllGroups();
}
