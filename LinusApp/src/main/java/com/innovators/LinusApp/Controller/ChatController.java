package com.innovators.LinusApp.Controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.innovators.LinusApp.POJO.ChatDetail;
import com.innovators.LinusApp.POJO.ChatGroup;
import com.innovators.LinusApp.Service.ChatService;

@RestController
@CrossOrigin("*")
public class ChatController {
	
	@RequestMapping(value="hello")
	public String sayHello(){
		return "Hello";
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

	@Autowired
	ChatService chatService;
	
	/* This method id invoked to get all the list of chat users */
	@RequestMapping(value="/groups", method = RequestMethod.GET)
	public List<ChatGroup> getGroupList(){
		
		LOGGER.info("::::::: Inside the method getGroupList method of ChatController ::::");
		return chatService.getGroupList();
		
	}
	
	/* This method is invoked to get the chatdetail of a particular group */
	@RequestMapping(value = "/chatdetail/{id}", method = RequestMethod.GET)
	public ChatDetail getChatDetailById(@PathVariable("id") int id){
		LOGGER.info("::::::: Inside the method getChatDetailById method of ChatController ::::");
		return chatService.getChatDetailById(id);
	}

}
