package com.innovators.LinusApp.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="group")
public class Group {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="groupid")
	private Integer groupid;
	
	@Column(name="groupname")
	private String groupname;

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public Group() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Group(Integer groupid, String groupname) {
		super();
		this.groupid = groupid;
		this.groupname = groupname;
	}


}
