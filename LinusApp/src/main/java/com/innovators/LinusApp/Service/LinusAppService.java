package com.innovators.LinusApp.Service;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innovators.LinusApp.Entities.Group;
import com.innovators.LinusApp.POJO.GroupData;
import com.innovators.LinusApp.Repository.LinusAppRepository;

@Service
public class LinusAppService {
	
	@Autowired
	private LinusAppRepository linusAppRepository;

	public GroupData getAllGroups() {
		GroupData groupData = new GroupData();
		Stream<Group> groupStream = linusAppRepository.findAllGroups();
		List<Group> groupLst = groupStream.collect(Collectors.toList());
		groupData.setGroupData(groupLst);
		return groupData;
	}
	
	public GroupData getGroupById(Integer groupId) {
		GroupData groupData = new GroupData();
		Stream<Group> groupStream = linusAppRepository.findGroup(groupId);
		List<Group> groupLst = groupStream.collect(Collectors.toList());
		groupData.setGroupData(groupLst);
		return groupData;
	}

	public void createGroup(Group group) {
		linusAppRepository.save(group);
	}

}
