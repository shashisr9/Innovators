package com.innovators.LinusApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.innovators.LinusApp.Controller.LinusAppController;
import com.innovators.LinusApp.Entities.Group;

@SpringBootApplication
public class LinusAppApplication implements CommandLineRunner{
	
	@Autowired
	private LinusAppController linusAppController;

	public static void main(String[] args) {
		SpringApplication.run(LinusAppApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
/*		linusAppController.createGroup(new Group(1, "GroupId1"));
		linusAppController.createGroup(new Group(2, "GroupId2"));
		linusAppController.createGroup(new Group(3, "GroupId3"));*/
		
	}
}
