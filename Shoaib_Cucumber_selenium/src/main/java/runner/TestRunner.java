package runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\resources\\features",
				 glue="stepdefinitions",
				 tags= {"@fail"},
				 //plugin="com.cucumber.listener.ExtentCucumberFormatter:Reports/report.html")
				 plugin= {"json:target/cucumber.json", "com.cucumber.listener.ExtentCucumberFormatter:Reports/report.html"})


public class TestRunner {
	
	public static Scenario scenario;
	@AfterClass
    public static void teardown() {
		

        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
       // Reporter.addScenarioLog("Scenario" + scenario.getName() + scenario.getStatus()); 
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("OS", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
        Reporter.setSystemInfo("Release", "Release 18.1");
}

}
