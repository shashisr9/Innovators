package stepdefinitions;
 
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

 
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
 
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
 
public class RESTAPI_stepdef {
 
	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;
    private String ENDPOINT_GET_BOOK_BY_ISBN = "https://www.googleapis.com/books/v1/volumes";
 
	@Given("a book exists with an isbn of (.*)")
	public void a_book_exists_with_isbn(String isbn){
		request = RestAssured.given().param("q", "isbn:" + isbn);
	}
 
	@When("a user retrieves the book by isbn")
	public void a_user_retrieves_the_book_by_isbn(){
		response = request.when().get(ENDPOINT_GET_BOOK_BY_ISBN);
		System.out.println("response: " + response.prettyPrint());
	}
 
	@Then("the status code is (\\d+)")
	public void verify_status_code(int statusCode) throws Exception{
		json = response.then().statusCode(statusCode);
		/*String text=json.extract().jsonPath().get("items[0].volumeInfo.publishedDate");
		boolean b3 = Pattern.matches("[0-9]{3}", text);  
		System.out.println(b3);
		
		if(b3==false)
		{
			throw new Exception("Failed as test contains 4 digits instead of three");
		}*/
		/*if(Integer.parseInt(text)>2000)
			System.out.println("Text" + "greater");*/
	}
 
	
	/**
	 * asserts on json fields with single values
	 */
	@And("response includes the following$")
	public void response_equals(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
				//assertThat(field.getKey(), equalTo(Integer.parseInt(field.getValue())));

				
			}
			else{
				json.body(field.getKey(), equalTo(field.getValue()));
			}
		}
	}
	
	
	/**
	 * asserts on json arrays
	 */
	@And("response includes the following in any order")
	public void response_contains_in_any_order(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), containsInAnyOrder(field.getValue()));
			}
		}
	}
 
}