package pageObjects;
 
 
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
 
public class GmailSignInPage {
	
	public GmailSignInPage(WebDriver driver) {
	    PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="identifier") 
	private WebElement emailID;
	
	@FindBy(xpath="//span[contains(text(),'Next')]") 
	private WebElement btn_next;
	
	@FindBy(xpath="//span[contains(text(), 'Forgot email?')]") 
	private WebElement link_frgtEmail;
	
	@FindBy(xpath="//span[contains(text(), 'Create account')]") 
	private WebElement link_createAccount;
	
	@FindBy(xpath="//span[contains(text(), 'Learn More')]") 
	private WebElement link_learnmore;
	
	@FindBy(xpath="//div[@id='lang-chooser']") 
	private WebElement listbox_language;
	
	@FindBy(xpath="//content[contains(text(), '‪English (United Kingdom)‬')]") 
	private WebElement listbox_value_en_uk;
	
	public void enter_emailID(String name) {
		emailID.sendKeys(name);
	}
	
	public void click_link_createAccount() {
		link_createAccount.click();
	}
	
	public void click_btn_Next() {
		btn_next.click();
	}
	
	public void click_link_frgtEmail() {
		link_frgtEmail.click();
	}
	
	public void click_link_learnmore() {
		link_learnmore.click();
	}
	
	public void click_lang_list() {
		listbox_language.click();
	}
	
	public void select_en_uk() {
		listbox_value_en_uk.click();
	}
	
	public void sign_in() {
		
		enter_emailID("shoaib.shahulhameed");
		click_btn_Next();
		
	}
 
}