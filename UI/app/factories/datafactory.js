(function(){
  'use strict';
  angular.module('myApp')
  .factory('dataFactory',dataFactory);

  dataFactory.$inject = ['$http','$httpParamSerializerJQLike'];

  function dataFactory($http,$httpParamSerializerJQLike){
    return {
      getGroups :getGroups,
      getChats  :getChats
    };

    function getGroups(){
      return $http.get('../mock/groups.json').then(function(response){
        console.log('Hit Successful');
        // console.log(response);
        return response;
      },function(response){
        console.log('Hit Unsuccessful');
        // console.log(response);
        return response;
      });
    }

    function getChats(groupId){
      // var url = ""
      return $http.get('../mock/chats.json').then(function(response){
        console.log('Hit Successful');
        // console.log(response);
        return response;
      },function(response){
        console.log('Hit Unsuccessful');
        // console.log(response);
        return response;
      });
    }
  }
})();
